from timeit import timeit  
from tabulate import tabulate 
import sys
import plistlib
import prt_pb2
import yaml

d = { 
   'words': """ 
        Lorem ipsum dolor sit amet, consectetur adipiscing 
        elit. Mauris adipiscing adipiscing placerat. 
        Vestibulum augue augue, 
        pellentesque quis sollicitudin id, adipiscing. 
        """, 
   'list': range(100), 
   'dict': dict((str(i),'a') for i in iter(range(3))),
   'int': 100, 
   'float': 100.123456 
}
 
message = '''d = { 
    'PackageID' : 1539, 
    'PersonID' : 33, 
    'Name' : """MEGA_GAMER_2222""", 
    'Inventory': dict((str(i),i) for i in iter(range(3))),   
    'CurrentLocation': """ 
        Pentos is a large port city, more populous than Astapor on Slaver Bay,  
        and may be one of the most populous of the Free Cities.  
        It lies on the bay of Pentos off the narrow sea, with the Flatlands  
        plains and Velvet Hills to the east. 
        The city has many square brick towers, controlled by the spice traders.  
        Most of the roofing is done in tiles. There is a large red temple in  
        Pentos, along with the manse of Illyrio Mopatis and the Sunrise Gate  
        allows the traveler to exit the city to the east,  
        in the direction of the Rhoyne. 
        """ 
}''' 

setup_proto     = '%s ; import prt_pb2; profile = prt_pb2.Msg(); profile.PackageID = d[\'PackageID\']; profile.PersonID = d[\'PersonID\']; profile.Name = d[\'Name\']; profile.CurrentLocation = d[\'CurrentLocation\']; profile.Inventory[1] = \'a\'; profile.Inventory[2] = \'a\'; profile.Inventory[3] = \'a\'; src = profile.SerializeToString()' % message
setup_pickle    = '%s ; import pickle ; src = pickle.dumps(d, 2)' % message 
setup_json      = '%s ; import json; src = json.dumps(d)' % message
setup_xml       = '%s ; import plistlib; src = plistlib.dumps(d)' % message
setup_yaml      = '%s ; import yaml; src = yaml.dump(d)' % message
 
tests = [ 
    # (title, setup, enc_test, dec_test) 
    ('pickle (native serialization)', 'import pickle; %s' % setup_pickle, 'pickle.dumps(d, 2)', 'pickle.loads(src)'),
    ('protobuf', 'import prt_pb2; %s' %setup_proto, 'profile.SerializeToString()', 'profile = prt_pb2.Msg(); profile.ParseFromString(src)'),
    ('json', 'import json; %s' % setup_json, 'json.dumps(d)', 'json.loads(src)'),
    ('xml', 'import plistlib; %s' % setup_xml, 'plistlib.dumps(d)', 'plistlib.loads(src)'),
    ('yaml', 'import yaml; %s' %setup_yaml, 'yaml.dump(d)', 'yaml.safe_load(src)')
] 
  
loops = 5000 
enc_table = [] 
dec_table = [] 
  
print ("Running tests (%d loops each)" % loops) 

for title, mod, enc, dec in tests: 
    print (title) 
  
    print ("  [Encode]", enc)  
    result = timeit(enc, mod, number=loops) 
    exec (mod) 
    enc_table.append([title, result, sys.getsizeof(src)]) 
  
    print ("  [Decode]", dec)  
    result = timeit(dec, mod, number=loops) 
    dec_table.append([title, result]) 
  
enc_table.sort(key=lambda x: x[1]) 
enc_table.insert(0, ['Package', 'Seconds', 'Size']) 
  
dec_table.sort(key=lambda x: x[1]) 
dec_table.insert(0, ['Package', 'Seconds']) 
  
print ("\nEncoding Test (%d loops)" % loops) 
print (tabulate(enc_table, headers="firstrow")) 
  
print ("\nDecoding Test (%d loops)" % loops) 
print (tabulate(dec_table, headers="firstrow")) 
