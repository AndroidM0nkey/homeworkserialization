FROM python:3-onbuild
RUN pip install pyyaml
RUN pip install tabulate simplejson
RUN pip install protobuf
COPY prt_pb2.py /
COPY script.py /
CMD ["python", "./script.py"]
